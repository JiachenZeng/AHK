/**
 * キーボード入力に音をつける
 */

~ESC::
~F1::
~F2::
~F3::
~F4::
~F5::
~F6::
~F7::
~F8::
~F9::
~F10::
~F11::
~F12::
~PrintScreen::
~Ins::
~`::
~1::
~2::
~3::
~4::
~5::
~6::
~7::
~8::
~9::
~0::
~-::
~=::
~Tab::
~q::
~w::
~e::
~r::
~t::
~y::
~u::
~i::
~o::
~p::
~[::
~]::
~\::
~a::
~s::
~d::
~f::
~g::
~h::
~j::
~k::
~l::
~;::
~'::
~Shift::
~z::
~x::
~c::
~v::
~b::
~n::
~m::
~,::
~.::
~/::
~Ctrl::
~LWin::
~Alt::
~Space::
~Left::
~Down::
~Up::
~Right::
    SoundPlay, sounds\any.wav
return

~BS:: SoundPlay, sounds\backspace.wav
~Enter:: SoundPlay, sounds\enter.wav

; メモリー消費が激しい
; SoundPlayEx(SoundFile) {
;     FileDelete, %A_ScriptDIR%\Sound1.AHK
;     FileAppend,
;     (
;     #NoTrayIcon
;     SoundPlay, %SoundFile%, Wait
;     ), %A_ScriptDir%\Sound1.AHK
;     Run, %A_ScriptDIR%\Sound1.AHK
; }