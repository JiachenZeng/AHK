#Requires AutoHotkey v2.0

#Include libs/Audio.ahk ; https://github.com/thqby/ahk2_lib/blob/master/Audio/Audio.ahk

Persistent()

;q:: ;MIDI play notes
hModule := DllCall("kernel32\LoadLibrary", "Str", "winmm", "Ptr")

; Initialize variables
hMIDIOutputDevice := 0
deviceName := "Cantabile Controller"
deviceFound := false

; Search for the device by name
AW := "W"
A_CharSize := 2
hWinMM := DllCall("kernel32\LoadLibrary" AW, "Str", "Winmm.dll", "Ptr")
NumDevs := DllCall("Winmm\midiOutGetNumDevs")
sz := 20 + 32 * A_CharSize
Loop NumDevs {
    DevCaps := Buffer(sz, 0)
    DllCall("Winmm\midiOutGetDevCaps" AW, "UInt", A_Index - 1, "Ptr", DevCaps, "UInt", sz)
    Name := StrGet(DevCaps.Ptr + 8)
    if (Name = deviceName) {
        vDeviceID := A_Index - 1
        deviceFound := true
        break
    }
}

if !deviceFound {
    MsgBox "Device '" deviceName "' not found."
    ExitApp
}

; Open the MIDI device
if DllCall("winmm\midiOutOpen", "Ptr*", &hMIDIOutputDevice, "UInt", vDeviceID, "UPtr", 0, "UPtr", 0, "UInt", 0, "UInt")
    ExitApp

hDev := hMIDIOutputDevice
vChan := 0

;==============================

; Function to send MIDI control message
MidiControlChange(hMIDIOutputDevice, Channel, ControlNo, Value) {
    DllCall("winmm\midiOutShortMsg", "UInt", hMIDIOutputDevice, "UInt", 0xB0 | Channel | (ControlNo << 8) | (Value << 16)) ; "Control Change" event
}

; Function to normalize volume level
NormalizeVolume(volume) {
    return Round(volume * 127 / 100)
}

; Callback function for volume change
SendMIDIVolumeMessage(data) {
    global hDev, vChan
    muted := data.bMuted
    volume := Round(data.masterVolume * 100)
    normalizedVolume := NormalizeVolume(volume)
    MidiControlChange(hDev, vChan, 99, normalizedVolume)
}

; Register the volume change callback
OnVolumeChange(SendMIDIVolumeMessage)

;==============================

OnExit(MIDICleanup)
MIDICleanup(*) {
    global hMIDIOutputDevice, hModule
    DllCall("winmm\midiOutClose", "Ptr", hMIDIOutputDevice, "UInt")
    DllCall("kernel32\FreeLibrary", "Ptr", hModule)
}

;==================================================

MidiNotePlay(hMIDIOutputDevice, vChannel, vNote, vVelocity, vDuration) {
    MidiNoteOn(hMIDIOutputDevice, vChannel, vNote, vVelocity)
    Sleep vDuration
    MidiNoteOff(hMIDIOutputDevice, vChannel, vNote, 0)
}

MidiNoteOn(hMIDIOutputDevice, vChannel, vNote, vVelocity) {
    DllCall("winmm\midiOutShortMsg", "Ptr", hMIDIOutputDevice, "UInt", 0x90 | vChannel | (vNote << 8) | (vVelocity << 16), "UInt")
}

MidiNoteOff(hMIDIOutputDevice, vChannel, vNote, vVelocity) {
    DllCall("winmm\midiOutShortMsg", "Ptr", hMIDIOutputDevice, "UInt", 0x80 | vChannel | (vNote << 8) | (vVelocity << 16), "UInt")
}

; Credit: https://www.reddit.com/r/AutoHotkey/comments/1alr736/comment/kpj3eiu/
; Function to bind volume change callback to the current default playback device
OnVolumeChange(function, AddRemove := 1) {
    static callbacks := Map(), init := 0
    static AEV, vt

    if callbacks.Has(function) && AddRemove = 0 {
        callbacks.Delete(function)
        return
    }

    callbacks.Set(function, 1)

    if !init {
        init := 1
        BindToDefaultDevice()
        ; Register for device change notifications
        RegisterDeviceChangeNotification()
        OnExit(UnRegister, -1)
    }

    AudioEndpointVolumeCallback(index, pInterface, params*) {
        switch index {
        case 0:
            static IID_IUnknown := "{00000000-0000-0000-C000-000000000046}"
            static IID_IAudioEndpointVolumeCallback := "{657804fa-d6ad-4496-8a60-352752af4f89}"

            VarSetStrCapacity(&iid, 38)
            DllCall("ole32\StringFromGUID2", "Ptr", params[1], "Str", iid, "Int", 39)
            if (iid = IID_IAudioEndpointVolumeCallback || iid = IID_IUnknown) {
                NumPut("ptr", pInterface, params[2])
                return 0 ; S_OK
            }
            NumPut("ptr", 0, params[2])
            return 0x80004002 ; E_NOINTERFACE
        case 3: ; OnNotify
            data := params[1]
            bMuted := NumGet(data, 16, "int")
            masterVolume := NumGet(data, 20, "float")
            for cb in callbacks
                cb.Call({bMuted: bMuted, masterVolume: masterVolume})
        default:
            return 0x80004001 ; E_NOTIMPL
        }
    }

    BindToDefaultDevice() {
        if IsSet(AEV)
            AEV.UnregisterControlChangeNotify(vt)

        de := IMMDeviceEnumerator()
        IMMD := de.GetDefaultAudioEndpoint()
        AEV := IMMD.Activate(IAudioEndpointVolume)

        ; Create IAudioEndpointVolumeCallback
        paramCounts := "3112"
        vt := Buffer((StrLen(paramCounts) + 1) * A_PtrSize)
        p := NumPut("ptr", vt.Ptr + A_PtrSize, vt)
        Loop Parse paramCounts
            p := NumPut("ptr", CallbackCreate(AudioEndpointVolumeCallback.Bind(A_Index - 1),, A_LoopField), p)

        AEV.RegisterControlChangeNotify(vt)
    }

    RegisterDeviceChangeNotification() {
        ; Create the notification client
        notificationClient := MyIMMNotificationClient()
        
        ; Register the notification client
        de := IMMDeviceEnumerator()
        de.RegisterEndpointNotificationCallback(notificationClient)
    }

    UnRegister(*) {
        if IsSet(AEV)
            AEV.UnregisterControlChangeNotify(vt)
    }
}

class MyIMMNotificationClient extends _interface_impl {
	static IID := "{7991EEC9-7E89-4D85-8390-6C703CEC60C0}"
	static vtable := [
		(this, iid, pobj) => !NumPut("ptr", this, pobj),
		(this) => 1,
		(this) => 1,
		["OnDeviceStateChanged", StrGet],
		["OnDeviceAdded", StrGet],
		["OnDeviceRemoved", StrGet],
		["OnDefaultDeviceChanged", , , StrGet],
		["OnPropertyValueChanged", StrGet],
	]

	/** @event */
	OnDeviceStateChanged(pwstrDeviceId, dwNewState) => 0
	/** @event */
	OnDeviceAdded(pwstrDeviceId) => 0
	/** @event */
	OnDeviceRemoved(pwstrDeviceId) => 0
	/** @event */
	OnDefaultDeviceChanged(flow, role, pwstrDefaultDeviceId) {
        MsgBox 123
        return 0
    }
	/** @event */
	OnPropertyValueChanged(pwstrDeviceId, key) => 0
}